# Fuzzy Time

Generate fuzz time between two dates.

## Quirks

Because browser does not support longs (Int64), every Date().getTime() has to be devided with 1000 to go from milisecond precision to second precision.

## Requirements

* Rust Nightly
* `rustup target add wasm32-unknown-unknown`
* `cargo +nightly install wasm-bindgen-cli`
 
## Run

Run `build.bat` and things should compile and npm should serve the page in a browser on <http://localhost:8080>