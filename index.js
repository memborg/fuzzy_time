const rust = import("./fuzzy_time");

const t_second = 1;
const t_minute = t_second * 60;
const t_hour = t_minute * 60;
const t_day = t_hour * 24;
const t_week = t_day * 7;
const t_month = Math.floor(t_day * 30.4);
const t_year = t_month * 12;

const now = Math.floor(Date.now() / 1000);

function fuzzy_time(sec) {
  let res = "";

  const dif = now - sec;

  const fuzzy_string = (time_ref, time_str) => {
    const fuzzy = Math.floor(dif / time_ref);

    res += fuzzy + " " + time_str;
    if (fuzzy != 1) {
      res += "s";
    }
    res += " ago";
  };

  if (dif >= t_year) fuzzy_string(t_year, "year");
  else if (dif >= t_month) fuzzy_string(t_month, "month");
  else if (dif >= t_week) fuzzy_string(t_week, "week");
  else if (dif >= t_day) fuzzy_string(t_day, "day");
  else if (dif >= t_hour) fuzzy_string(t_hour, "hour");
  else if (dif >= t_minute) fuzzy_string(t_minute, "minute");
  else if (dif >= t_second) fuzzy_string(t_second, "second");
  else res = "now";

  return res;
}

function getRandomDateBetween(dateMin, dateMax) {
  const min = dateMin.getTime(),
    max = dateMax.getTime();
  return new Date(Math.floor(Math.random() * (max - min + 1)) + min);
}

function getAvg(arr) {
  var sum = arr.reduce(function(a, b) {
    return a + b;
  });
  return sum / arr.length;
}

function init() {
  var dates = [];
  var avgJSTimes = [];
  var avgTimes = [];

  var max = new Date();
  var min = new Date(2001, 1, 1);

  var maxCount = 1000000;

  console.log("Generate dates");
  for (var i = 0; i < maxCount; i++) {
    dates.push(getRandomDateBetween(min, max));
  }

  console.log(maxCount, "date elements");

  console.time("js.fuzzy_time");
  dates.forEach(function(date) {
    var s = new Date();

    fuzzy_time(date.getTime() / 1000);

    var e = new Date();

    avgJSTimes.push(e.getTime() - s.getTime());
  });
  console.timeEnd("js.fuzzy_time");

  var avg = getAvg(avgJSTimes);

  console.log("JS average", avg, "millisecond");

  rust.then(m => {
    console.time("wasm.fuzzy_time");
    dates.forEach(function(date) {
      var s = new Date();
      m.fuzzy_time(date.getTime() / 1000, now);
      var e = new Date();

      avgTimes.push(e.getTime() - s.getTime());
    });
    console.timeEnd("wasm.fuzzy_time");

    var avg = getAvg(avgTimes);

    console.log("WASM average", avg, "millisecond");
  });
}

init();
