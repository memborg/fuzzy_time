extern crate wasm_bindgen;
use wasm_bindgen::prelude::*; 

const T_SECOND: i64 = 1;
const T_MINUTE: i64 = T_SECOND * 60;
const T_HOUR: i64 = T_MINUTE * 60;
const T_DAY: i64 = T_HOUR * 24;
const T_WEEK: i64 = T_DAY * 7;
const T_MONTH: i64 = T_DAY * 30;
const T_YEAR: i64 = T_MONTH * 12;

#[wasm_bindgen]
pub fn fuzzy_time(then: i32, now: i32) -> String {

    let now = now as i64;
    let diff = now - (then as i64);

    let mut fuzzy = fuzzy_struct(T_SECOND, diff, 0);

    if diff >= T_YEAR {
        fuzzy = fuzzy_struct(T_YEAR, diff, 7);
    } else if diff >= T_MONTH {
        fuzzy = fuzzy_struct(T_MONTH, diff, 6);
    } else if diff >= T_WEEK {
        fuzzy = fuzzy_struct(T_WEEK, diff, 5);
    } else if diff >= T_DAY {
        fuzzy = fuzzy_struct(T_DAY, diff, 4);
    } else if diff >= T_HOUR {
        fuzzy = fuzzy_struct(T_HOUR, diff, 3);
    } else if diff >= T_MINUTE {
        fuzzy = fuzzy_struct(T_MINUTE, diff, 2);
    } else if diff >= T_SECOND {
        fuzzy = fuzzy_struct(T_SECOND, diff, 1);
    }

    fuzzy
}

fn fuzzy_struct(time_ref: i64, diff: i64, time_type: i32) -> String {
    format!("age:{age},type:{type_ref}", age = (diff/time_ref), type_ref = time_type)
}
